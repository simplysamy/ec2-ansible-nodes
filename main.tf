resource "aws_default_vpc" "default" {}
resource "aws_instance" "my_instances" {
    count = var.instance_count
    ami = var.ami 
    instance_type = var.aws_instance_type
    vpc_security_group_ids = [ aws_security_group.ansible_node_sg.id ]
    key_name = var.key_name
    user_data = <<-EOF
                #!/bin/bash 
                sudo apt update
                chmod 700 ~/.ssh
                chmod 600 ~/.ssh/authorized_keys
                EOF

    tags = {
        Name = "my-instance-${count.index + 1}"
    }
}

resource "aws_security_group" "ansible_node_sg" {
  name_prefix = "instance_sg"
  description = "Allow inbound traffic on port 22, 80 & 8080"
  vpc_id      = aws_default_vpc.default.id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

# Paste public key in authorized_keys file of newly created VMs
resource "null_resource" "copy_ssh_key" {
    depends_on = [aws_instance.my_instances]
    
    # Add count to replicate the resource based on the number of instances.
    count = var.instance_count

    connection {
        type = "ssh"
        host = aws_instance.my_instances[count.index].public_ip
        user = "ubuntu" 
        private_key = file("~/mtc-terransible.pem") # Change to your private key path
    } 
    
    provisioner "remote-exec" {
      inline = [
            "echo '${file("~/.ssh/id_rsa.pub")}' >> ~/.ssh/authorized_keys",  # Change to your public key
             "echo 'Copy Completed'"
      ]
    }
   
}

