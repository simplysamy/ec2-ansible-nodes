output "public_ip_addresses" {
  value = join(", ", aws_instance.my_instances[*].public_ip)
}

output "private_ip_addresses" {
  value = join(", ", aws_instance.my_instances[*].private_ip)
}
